﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Management;


public class StartingLogo : MonoBehaviour
{
    private const string Key = "Serial";

    [SerializeField]
    private RawImage logoImage;
    private double timer;


    void Start()
    {
        
        if (PlayerPrefs.GetString(Key) != null || !PlayerPrefs.GetString(Key).Equals(string.Empty))
        {
            string str = PlayerPrefs.GetString(Key);

            if (!PlayerPrefs.GetString(Key).Equals(GetHDDSerial().GetHashCode().ToString()))
            {
                Application.Quit();
            }

            timer = 0f;
        } else
        {
            PlayerPrefs.SetString(Key, GetHDDSerial().GetHashCode().ToString());
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (timer > 5f && logoImage.color.a > 0)
        {
            Color newColor = logoImage.color;
            newColor.a -= Time.deltaTime;
            logoImage.color = newColor;
        }

        if (timer < 2f && logoImage.color.a < 1f)
        {
            Color newColor = logoImage.color;
            newColor.a += Time.deltaTime;
            logoImage.color = newColor;
        }

        if (timer > 7f)
        {
            SceneManager.LoadScene(1);
        }

        timer += Time.deltaTime;
    }

    public string GetHDDSerial()
    {
        ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_PhysicalMedia");

        foreach (ManagementObject wmi_HD in searcher.Get())
        {
            // get the hardware serial no.
            if (wmi_HD["SerialNumber"] != null)
                return wmi_HD["SerialNumber"].ToString();
        }

        return string.Empty;
    }

}
