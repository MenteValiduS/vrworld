﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;

public class LogoVideoPlayer : MonoBehaviour
{
    private VideoPlayer videoPlayer;
    private RawImage rawImage;
    private bool videoStarted;


    private void Update()
    {
        
        if ((videoPlayer.length - videoPlayer.time) < 1f && rawImage.color.a > 0)
        {
            var newAlphaColor = rawImage.color;
            newAlphaColor.a -= Time.deltaTime;
            rawImage.color = newAlphaColor;
        }

        if (videoStarted && !videoPlayer.isPlaying)
        {
            SceneManager.LoadScene(1);
        }
        //if (Input.anyKeyDown || videoPlayer.isPaused)
        //{
        //    Destroy(gameObject);
        //}

    }

    private void Start()
    {
        videoPlayer = gameObject.GetComponent<VideoPlayer>();
        //videoPlayer.url = SettingsDAO.SettingsContainer.settings.adVideoPath;
        rawImage = gameObject.GetComponent<RawImage>();
        rawImage.texture = videoPlayer.texture;
        videoStarted = false;
        StartCoroutine(PlayTrailer());
    }



    IEnumerator PlayTrailer()
    {
        videoPlayer.Prepare();

        if (!videoPlayer.isPrepared)
        {
            yield return new WaitForSeconds(1);
        }

        videoStarted = true;
        rawImage.texture = videoPlayer.texture;
        videoPlayer.Play();
    }
}
