﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class AddingGameArea : AreaChooser
{
    [SerializeField]
    private GameObject lastField;
    [SerializeField]
    private InputField name;
    [SerializeField]
    private InputField pathToGame;
    [SerializeField]
    private InputField pathToImage;
    [SerializeField]
    private InputField pathToVideo;
    [SerializeField]
    private InputField description;
    [SerializeField]
    private Dropdown genresDropdown;
    [SerializeField]
    private Text previewNameText;
    [SerializeField]
    private Image previewImage;
    [SerializeField]
    private VideoPlayer previwTrailerVideoPlayer;

    private void Start()
    {
        List<string> options = new List<string>();
        genresDropdown.ClearOptions();
        options.Add("Все");

        foreach (Genre genre in GenresDAO.Genres.genres)
        {
            options.Add(genre.name);
        }

        genresDropdown.AddOptions(options);
    }


    public void Add()
    {

        if (Check())
        {

            if (genresDropdown.options.Count != 0)
            {
                GamesDAO.AddGame(name.text, pathToGame.text, pathToImage.text, pathToVideo.text,
                    genresDropdown.options[genresDropdown.value].text, description.text);
            }
            else
            {
                GamesDAO.AddGame(name.text, pathToGame.text, pathToImage.text, pathToVideo.text,
                    "Все", description.text);
            }

            GamesDAO.SaveGames();
            //GamePanel.IsToUpdate = true;
            MessageSystem.MessageText = "Игра успешно добавлена!";
            MessageSystem.ShowDialog();
            Clear();
        }
        else
        {
            MessageSystem.IsToShow = true;
            return;
        }

    }

    private void Clear()
    {
        name.text = string.Empty;
        name.placeholder.color = Color.gray;
        name.textComponent.color = Color.black;
        pathToGame.text = string.Empty;
        pathToGame.placeholder.color = Color.gray;
        pathToGame.textComponent.color = Color.black;
        pathToImage.text = string.Empty;
        pathToImage.placeholder.color = Color.gray;
        pathToImage.textComponent.color = Color.black;
        pathToVideo.text = string.Empty;
        pathToVideo.placeholder.color = Color.gray;
        pathToVideo.textComponent.color = Color.black;
        description.text = string.Empty;
        GameObject.Find("PreviewField").GetComponent<PreviewField>().Clear();
        //previewNameText.text = string.Empty;
        //previewImage.sprite = null;
        //previwTrailerVideoPlayer.Stop();
        //description.textComponent.color = Color.black;
    }

    private bool Check()
    {
        bool res = true;
        MessageSystem.MessageText = string.Empty;

        if (name.text.Equals(string.Empty))
        {
            name.placeholder.color = Color.red;
            res = false;
            MessageSystem.MessageText += "Введите название игры!\n";
        }
        else if (GamesDAO.GetIDByName(name.text) != -1)
        {
            name.placeholder.color = Color.red;
            res = false;
            MessageSystem.MessageText += "Игра с таким названием уже существует!\n";
        }
        else
        {
            name.placeholder.color = Color.gray;
            name.textComponent.color = Color.black;
        }

        if (pathToGame.text.Equals(string.Empty))
        {
            pathToGame.placeholder.color = Color.red;
            res = false;
            MessageSystem.MessageText += "Укажите путь к exe-файлу!\n";
        }
        else if (!File.Exists(pathToGame.text))
        {
            pathToGame.textComponent.color = Color.red;
            res = false;
            MessageSystem.MessageText += "Такого exe-файла не существует!\n";
        }
        else
        {
            pathToGame.placeholder.color = Color.gray;
            pathToGame.textComponent.color = Color.black;
        }

        if (pathToImage.text.Equals(string.Empty))
        {
            pathToImage.placeholder.color = Color.red;
            res = false;
            MessageSystem.MessageText += "Укажите путь к изображению-превью!\n";
        }
        else if (!File.Exists(pathToImage.text))
        {
            pathToImage.textComponent.color = Color.red;
            res = false;
            MessageSystem.MessageText += "Такого файла (изображение-превью) не существует!\n";
        }
        else
        {
            pathToImage.placeholder.color = Color.gray;
            pathToImage.textComponent.color = Color.black;
        }

        if (pathToVideo.text.Equals(string.Empty))
        {
            pathToVideo.placeholder.color = Color.red;
            res = false;
            MessageSystem.MessageText += "Укажите путь к трейлеру!\n";
        }
        else if (!File.Exists(pathToVideo.text))
        {
            pathToVideo.textComponent.color = Color.red;
            res = false;
            MessageSystem.MessageText += "Такого файла (трейлера) не существует!";
        }
        else
        {
            pathToVideo.placeholder.color = Color.gray;
            pathToVideo.textComponent.color = Color.black;
        }

        return res;
    }

    public void Back()
    {
        GamePanel.IsToUpdate = true;
        Destroy(gameObject);
        GameObject editingPanelObject = GameObject.Find("EditingPanel(Clone)");
        EditingPanel editingPanel = editingPanelObject.GetComponent<EditingPanel>();
        editingPanel.SetSmallWindow();
        Instantiate(lastField, editingPanelObject.transform);
    }

    public void Close()
    {
        GamePanel.IsToUpdate = true;
        base.Close();
    }

}
