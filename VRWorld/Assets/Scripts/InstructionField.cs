﻿using UnityEngine;
using UnityEngine.UI;

public class InstructionField : MonoBehaviour
{
    [SerializeField]
    private Image instructionImage;

    private void Start()
    {

        if (!System.IO.File.Exists(SettingsDAO.SettingsContainer.settings.instructionImagePath))
        {
            instructionImage.sprite = Resources.Load<Sprite>("Images/instruction");
        }
        else
        {
            instructionImage.sprite = IMG2Sprite.LoadNewSprite(SettingsDAO.SettingsContainer.settings.instructionImagePath);
        }

    }

    void Update()
    {
        if (Input.anyKeyDown)
        {
            Destroy(gameObject);
            Destroy(GameObject.Find("CoveringField(Clone)"));
        }
    }
}
