﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EditingGameArea : AreaChooser
{
    [SerializeField]
    private GameObject lastField;
    [SerializeField]
    private Dropdown gamesDropdown;
    [SerializeField]
    private InputField nameInput;
    [SerializeField]
    private Dropdown genresDropdown;
    [SerializeField]
    private InputField pathToGameInput;
    [SerializeField]
    private InputField pathToImageInput;
    [SerializeField]
    private InputField pathToVideoInput;
    [SerializeField]
    private InputField descriptionInput;

    private int idOfSelectedGame;

    void Start()
    {
        Refresh();
        GameObject.Find("PreviewField").GetComponent<PreviewField>().SetAll();
    }

    private void Refresh()
    {
        List<string> options = new List<string>();
        gamesDropdown.ClearOptions();

        foreach (Game game in GamesDAO.Games.games)
        {
            options.Add(game.name);
        }

        gamesDropdown.AddOptions(options);

        options.Clear();
        genresDropdown.ClearOptions();
        options.Add("Все");

        foreach (Genre genre in GenresDAO.Genres.genres)
        {
            options.Add(genre.name);
        }

        genresDropdown.AddOptions(options);

        SetGameData();
    }

    public void SetGameData()
    {
        idOfSelectedGame = GamesDAO.GetIDByName(gamesDropdown.options[gamesDropdown.value].text);
        Game infoOfSelectedGame = GamesDAO.Games.games[idOfSelectedGame];
        nameInput.text = infoOfSelectedGame.name;
        // Установка значения выбранного жанра
        int index = 0;
        
        for (int i = 0; i < genresDropdown.options.Count; i++)
        {
            if (genresDropdown.options[i].text.Equals(infoOfSelectedGame.genre))
            {
                index = i;
                break;
            }
        }

        genresDropdown.value = index;
        pathToGameInput.text = infoOfSelectedGame.pathToExe;
        pathToImageInput.text = infoOfSelectedGame.pathToImage;
        pathToVideoInput.text = infoOfSelectedGame.pathToTrailer;
        descriptionInput.text = infoOfSelectedGame.description;

        GameObject.Find("PreviewField").GetComponent<PreviewField>().SetAll();
    }


    public void ChangeData()
    {
        if (gamesDropdown.options.Count != 0)
        {

            
            if (Check())
            {
                Game game;
                if (genresDropdown.options.Count != 0)
                {
                    game = new Game(nameInput.text, pathToImageInput.text, pathToGameInput.text, genresDropdown.options[genresDropdown.value].text, pathToVideoInput.text, descriptionInput.text);
                }
                else
                {
                    game = new Game(nameInput.text, pathToImageInput.text, pathToGameInput.text, "Все", pathToVideoInput.text, descriptionInput.text);
                }
                GamesDAO.ChangeGameData(idOfSelectedGame, game);
                GamesDAO.SaveGames();
                GamePanel.IsToUpdate = true;
                ResetColors();
                MessageSystem.ShowDialog("Игра успешно изменена!");
            }
            else
            {
                MessageSystem.IsToShow = true;
                return;
            }
        }
        else
        {
            MessageSystem.ShowDialog("Прежде чем изменять игру - добавьте её в список!");
        }
    }

    private void ResetColors()
    {
        nameInput.placeholder.color = Color.gray;
        nameInput.textComponent.color = Color.black;
        pathToGameInput.placeholder.color = Color.gray;
        pathToGameInput.textComponent.color = Color.black;
        pathToImageInput.placeholder.color = Color.gray;
        pathToImageInput.textComponent.color = Color.black;
        pathToVideoInput.placeholder.color = Color.gray;
        pathToVideoInput.textComponent.color = Color.black;
        descriptionInput.textComponent.color = Color.black;
    }

    private bool Check()
    {
        bool res = true;
        MessageSystem.MessageText = string.Empty;

        if (nameInput.text.Equals(string.Empty))
        {
            nameInput.placeholder.color = Color.red;
            res = false;
            MessageSystem.MessageText += "Введите название игры!\n";
        }
        else if (GamesDAO.GetIDByName(nameInput.text) != -1)
        {
            int index = GamesDAO.GetIDByName(nameInput.text);

            if (!GamesDAO.Games.games[index].name.Equals(gamesDropdown.options[gamesDropdown.value].text))
            {
                nameInput.placeholder.color = Color.red;
                res = false;
                MessageSystem.MessageText += "Игра с таким названием уже существует!\n";
            }
            
        }

        if (pathToGameInput.text.Equals(string.Empty))
        {
            pathToGameInput.placeholder.color = Color.red;
            res = false;
            MessageSystem.MessageText += "Укажите путь к exe-файлу!\n";
        }
        else if (!System.IO.File.Exists(pathToGameInput.text))
        {
            pathToGameInput.textComponent.color = Color.red;
            res = false;
            MessageSystem.MessageText += "Такого exe-файла не существует!\n";
        }

        if (pathToImageInput.text.Equals(string.Empty))
        {
            pathToImageInput.placeholder.color = Color.red;
            res = false;
            MessageSystem.MessageText += "Укажите путь к изображению-превью!\n";
        }
        else if (!System.IO.File.Exists(pathToImageInput.text))
        {
            pathToImageInput.textComponent.color = Color.red;
            res = false;
            MessageSystem.MessageText += "Такого файла (изображение-превью) не существует!\n";
        }

        if (pathToVideoInput.text.Equals(string.Empty))
        {
            pathToVideoInput.placeholder.color = Color.red;
            res = false;
            MessageSystem.MessageText += "Укажите путь к трейлеру!\n";
        }
        else if (!System.IO.File.Exists(pathToVideoInput.text))
        {
            pathToVideoInput.textComponent.color = Color.red;
            res = false;
            MessageSystem.MessageText += "Такого файла (трейлера) не существует!\n";
        }

        return res;
    }

    public void Back()
    {
        GamePanel.IsToUpdate = true;
        Destroy(gameObject);
        GameObject editingPanelObject = GameObject.Find("EditingPanel(Clone)");
        EditingPanel editingPanel = editingPanelObject.GetComponent<EditingPanel>();
        editingPanel.SetSmallWindow();
        Instantiate(lastField, editingPanelObject.transform);
    }
    public void Close()
    {
        GamePanel.IsToUpdate = true;
        base.Close();
    }

}
