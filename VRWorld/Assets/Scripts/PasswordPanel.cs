﻿using UnityEngine;
using UnityEngine.UI;

public class PasswordPanel : MonoBehaviour
{
    [SerializeField]
    private InputField input;

    private string password = "1861";

    private void Start() 
    {
        input.contentType = InputField.ContentType.Password;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            Accept();
        }
    }

    public void Accept()
    {

        if (input.text.Equals(password))
        {
            Destroy(gameObject);
        }
        else
        {
            MessageSystem.ShowDialog("Неверный пароль!");
        }

    }

    public void Close()
    {
        Destroy(GameObject.Find("CoveringField(Clone)"));
    }

}
