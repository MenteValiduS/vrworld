﻿using UnityEngine;
using UnityEngine.UI;

public class LockPanel : MonoBehaviour
{
    [SerializeField]
    private InputField input;

    private string password;

    private void Start()
    {
        input.contentType = InputField.ContentType.Password;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            Accept();
        }
    }

    public void Accept()
    {

        if (SettingsDAO.SettingsContainer.settings.customerPassword == null || 
            SettingsDAO.SettingsContainer.settings.customerPassword.Equals(string.Empty))
        {
            SettingsDAO.SettingsContainer.settings.customerPassword = "0000";
        }

        if (input.text.Equals(SettingsDAO.SettingsContainer.settings.customerPassword))
        {
            Destroy(GameObject.Find("CoveringField(Clone)"));
        }
        else
        {
            MessageSystem.ShowDialog("Неверный пароль!");
        }

    }

}
