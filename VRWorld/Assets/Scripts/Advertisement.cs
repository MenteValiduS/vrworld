﻿using UnityEngine;

public class Advertisement : MonoBehaviour
{
    [SerializeField]
    private GameObject adField;
    private static bool toUpdate;
    private bool advertised;
    private double lastCheckedTime;

    public static bool ToUpdate { private get => toUpdate; set => toUpdate = value; }

    private void Start()
    {
        lastCheckedTime = Timer.Time;
        advertised = false;
        ToUpdate = false;
    }

    private void Update()
    {

        if (toUpdate)
        {
            lastCheckedTime = Timer.Time;
            toUpdate = false;
        }

        if (!PlayGame.Launched && SettingsDAO.SettingsContainer.settings.adTime != 0)
        {

            if (Input.anyKeyDown)
            {
                lastCheckedTime = Timer.Time;

                if (advertised)
                {
                    GameObject ad = GameObject.Find("AdvertiseField(Clone)");

                    if (ad != null)
                    {
                        Destroy(ad);
                    }

                    advertised = false;
                }

            }

            if (!advertised && (lastCheckedTime + SettingsDAO.SettingsContainer.settings.adTime * 60d) - Timer.Time < double.Epsilon)
            {
                Instantiate(adField, gameObject.transform);
                advertised = true;
            }

        }

    }

}
