﻿using UnityEngine;
using System.IO;
using System.Collections.Generic;

public class IMG2Sprite
{

    // This script loads a PNG or JPEG image from disk and returns it as a Sprite
    // Drop it on any GameObject/Camera in your scene (singleton implementation)
    //
    // Usage from any other script:
    // MySprite = IMG2Sprite.LoadNewSprite(FilePath, [PixelsPerUnit (optional)])
    private static Dictionary<string, Sprite> spritePull;
   

    public static Sprite LoadNewSprite(string FilePath, float PixelsPerUnit = 100.0f)
    {

        if (spritePull == null || spritePull != null && !spritePull.ContainsKey(FilePath))
        {
            // Load a PNG or JPG image from disk to a Texture2D, assign this texture to a new sprite and return its reference

            Texture2D SpriteTexture = LoadTexture(FilePath);

            if (SpriteTexture == null)
            {
                return Resources.Load<Sprite>("GamesLogo/NoImage");
            }
            Sprite sprite = Sprite.Create(SpriteTexture, new Rect(0, 0, SpriteTexture.width, SpriteTexture.height), new Vector2(0, 0), PixelsPerUnit);

            if (spritePull == null)
            {
                spritePull = new Dictionary<string, Sprite>();
            }

            spritePull.Add(FilePath, sprite);

            return sprite;

        }
        else if (spritePull != null)
        {
            return spritePull[FilePath];
        }
        else
        {
            return null;
        }

    }

    private static Texture2D LoadTexture(string FilePath)
    {

        // Load a PNG or JPG file from disk to a Texture2D
        // Returns null if load fails

        Texture2D Tex2D;
        byte[] FileData;

        if (File.Exists(FilePath))
        {
            FileData = File.ReadAllBytes(FilePath);
            Tex2D = new Texture2D(2, 2);           // Create new "empty" texture
            if (Tex2D.LoadImage(FileData))           // Load the imagedata into the texture (size is set automatically)
                return Tex2D;                 // If data = readable -> return texture
        }
        return null;                     // Return null if load failed
    }
}