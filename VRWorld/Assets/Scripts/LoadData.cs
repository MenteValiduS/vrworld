﻿using DB;
using System.IO;
using UnityEngine;

public class LoadData : MonoBehaviour
{
    void Awake()
    {
        if (!File.Exists(Path.Combine(Application.dataPath, "VWDB.db")))
        {

        }

        SettingsDAO.LoadSettings();

        if (File.Exists(Path.Combine(Application.dataPath, "games.json")))
        {
            JsonDAO.JsonPath = "games.json";
            GameData Games = JsonDAO.Load<GameData>();

            foreach (Game game in Games.games)
            {
                if (DataBase.GameInsertCheck(game.name))
                {
                    DataBase.Insert(game);
                    game.ID = DataBase.GetGameID(game.name);
                }
            }

            File.Delete(Path.Combine(Application.dataPath, "games.json"));
        }

        GamesDAO.LoadGames();

        if (File.Exists(Path.Combine(Application.dataPath, "genres.json")))
        {
            JsonDAO.JsonPath = "genres.json";
            GenreContainer Genres = JsonDAO.Load<GenreContainer>();

            foreach (Genre genre in Genres.genres)
            {
                if (DataBase.GenreInsertCheck(genre.name))
                {
                    DataBase.Insert(genre);
                    genre.ID = DataBase.GetGenreID(genre.name);
                }
            }

            File.Delete(Path.Combine(Application.dataPath, "genres.json"));
        }

        GenresDAO.LoadGenres();
        //GamesDAO.LoadGames();
    }
}
