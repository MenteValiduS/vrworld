﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePanel : MonoBehaviour
{

    private int indexOfTheFirstGame;
    private List<Game> currentSetOfGames;
    //private int amountOfGamesOnScreen;
    private static bool isToUpdate;

    public static bool IsToUpdate { get => isToUpdate; set => isToUpdate = value; }

    private void Start()
    {
        IsToUpdate = false;
        //amountOfGamesOnScreen = ButtonFinder.Buttons.Count;
        indexOfTheFirstGame = 0;
        SortGames();
        SetGames();
    }

    private void Update()
    {
        if (IsToUpdate)
        {
            indexOfTheFirstGame = 0;
            SortGames();
            SetGames();
            IsToUpdate = false;
        }
    }

    private void SortGames()
    {
        string genreName = GenrePanel.GenreNameText.text;
        currentSetOfGames = new List<Game>();
        currentSetOfGames = GamesDAO.GetGames(GenrePanel.GenreNameText.text, indexOfTheFirstGame, ButtonFinder.Buttons.Count);
        Debug.Log(indexOfTheFirstGame + " + " + currentSetOfGames.Count + " = " + (indexOfTheFirstGame + currentSetOfGames.Count));
        //Debug.Log("Метод SortGames: Текущие игры ");
        //foreach (Game game in currentSetOfGames)
        //{
       //     Debug.Log(game.name);
        //}
    }

    

    private void SetGames()
    {

        if (currentSetOfGames.Count == 0  && indexOfTheFirstGame == 0)
        {

            for (int i = 0; i < ButtonFinder.Buttons.Count; i++)
            {
                HandleButton(i, true);
            }

        }
        else
        {

            if (currentSetOfGames.Count != 0)
            {
                for (int i = 0; i < ButtonFinder.Buttons.Count; i++)
                {

                    if (i < currentSetOfGames.Count)
                    {
                        HandleButton(i);
                    }
                    else
                    {
                        HandleButton(i, true);
                    }

                }
            }

        }

    }

    private void HandleButton(int index, bool isToClear = false)
    {
        Text text;
        Image icon;
        GameButton gameInfo;

        if (isToClear)
        {
            ClearFields(ButtonFinder.Buttons[index]);

            text = ButtonFinder.Buttons[index].GetComponentInChildren<Text>();
            text.text = "Пустой слот";

            icon = ButtonFinder.Buttons[index].GetComponentInChildren<Image>();
            icon.sprite = Resources.Load<Sprite>(@"GamesLogo/NoImage");
        }
        else
        {
            gameInfo = ButtonFinder.Buttons[index].GetComponent<GameButton>();
            CopyValuesFields(currentSetOfGames[index], gameInfo);

            text = ButtonFinder.Buttons[index].GetComponentInChildren<Text>();
            text.text = gameInfo.name;

            icon = ButtonFinder.Buttons[index].GetComponentInChildren<Image>();
            icon.sprite = IMG2Sprite.LoadNewSprite(gameInfo.pathToImage);
        }
    }

    private void ClearFields(Button button)
    {
        GameButton gameInfo = button.GetComponent<GameButton>();
        gameInfo.description = string.Empty;
        gameInfo.genre = string.Empty;
        gameInfo.name = string.Empty;
        gameInfo.pathToExe = string.Empty;
        gameInfo.pathToImage = string.Empty;
        gameInfo.pathToTrailer = string.Empty;
    }

    private void CopyValuesFields(Game sender, GameButton recipient)
    {
        recipient.description = sender.description;
        recipient.genre = sender.genre;
        recipient.name = sender.name;
        recipient.pathToExe = sender.pathToExe;
        recipient.pathToImage = sender.pathToImage;
        recipient.pathToTrailer = sender.pathToTrailer;
    }

    private int GetIndexOfLastSetOfGames()
    {
        int count = GamesDAO.GetGamesCount(GenrePanel.GenreNameText.text);
        int div = (int)(count / ButtonFinder.Buttons.Count);
        int remOfDiv = count % ButtonFinder.Buttons.Count;

        if (0 == remOfDiv)
        {
            div--;
        }

        Debug.Log("Метод GetIndexOfLastSetOfGames вернул: " + div * ButtonFinder.Buttons.Count);

        return div * ButtonFinder.Buttons.Count ;
    }

    public void GetLeftSetOfGames()
    {
        //Debug.Log(GamesDAO.Games.games.Count + " < " + ButtonFinder.Buttons.Count + "?");
        if (GamesDAO.GetGamesCount(GenrePanel.GenreNameText.text) < ButtonFinder.Buttons.Count)
        {
            return;
        }
        else
        {
            //Debug.Log("Метод GetLeftSetOfGames: indexOfTheFirstGame = " + indexOfTheFirstGame);
            if (0 == indexOfTheFirstGame)
            {
                indexOfTheFirstGame = GetIndexOfLastSetOfGames();
                //Debug.Log("Следуюющие 8 игр. Первый индекс:" + indexOfTheFirstGame + "\n");
            }
            else
            {
                indexOfTheFirstGame -= ButtonFinder.Buttons.Count;
                //Debug.Log("Следуюющие 8 игр. Первый индекс:" + indexOfTheFirstGame + "\n");
            }

        }

        SortGames();
        SetGames();
    }

    public void GetRightSetOfGames()
    {
        if (GamesDAO.GetGamesCount(GenrePanel.GenreNameText.text) < ButtonFinder.Buttons.Count)
        {
            return;
        }
        else
        {
            
            if (null == currentSetOfGames)
            {
                currentSetOfGames = GamesDAO.GetGames(GenrePanel.GenreNameText.text, indexOfTheFirstGame, ButtonFinder.Buttons.Count);
            }

            if (currentSetOfGames.Count < ButtonFinder.Buttons.Count)
            {
                indexOfTheFirstGame = 0;
            }
            else
            {
                indexOfTheFirstGame += currentSetOfGames.Count;

                if (indexOfTheFirstGame + 1 > GamesDAO.GetGames().Count)
                {
                    indexOfTheFirstGame -= currentSetOfGames.Count;
                }
            }

        }

        SortGames();
        SetGames();
    }
}
