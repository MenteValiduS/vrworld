﻿using UnityEngine;

public class Calculations
{


    private static float a1, b1, a2, b2;

    private static float maxX;
    public static float MaxX
    {
        get
        {
            return maxX;
        }

        set
        {
            maxX = value;
        }
    }

    private static float maxY;
    public static float MaxY
    {
        get
        {
            return maxY;
        }

        set
        {
            maxY = value;
        }
    }

    public static float GetValueOfSetOfEquation(float curX)
    {
        Vector2 A = new Vector2(MaxX, MaxY * 3 / 4);
        Vector2 B = new Vector2(MaxX * 1 / 10, MaxY);
        Vector2 C = new Vector2(0f, 0f);
        GetValueOfLinearEquation(A, B, C);

        if (curX < B.x)
        {
            return a1 * curX + b1;
        } else
        {
            return a2 * curX + b2;
        }
    }

    private static void GetValueOfLinearEquation(Vector2 A, Vector2 B, Vector2 C)
    {
        a1 = (B.y - A.y) / (B.x - A.x);
        b1 = A.y - a1 * A.x;

        a2 = (C.y - B.y) / (C.x - B.x);
        b2 = B.y - a2 * B.x;
    }

}
