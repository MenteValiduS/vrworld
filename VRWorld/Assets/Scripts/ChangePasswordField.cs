﻿using UnityEngine;
using UnityEngine.UI;

public class ChangePasswordField : MonoBehaviour
{
    [SerializeField]
    private InputField oldPassword_InputField;
    [SerializeField]
    private InputField newPassword_InputField;

    void Update()
    {

        if (Input.GetKey(KeyCode.Return))
        {
            Accept();
        }

    }

    public void Accept()
    {
        Debug.Log(SettingsDAO.SettingsContainer.settings.customerPassword);

        if (SettingsDAO.SettingsContainer.settings.customerPassword == null)
        {
            SettingsDAO.SettingsContainer.settings.customerPassword = "0000";
        }

        if (oldPassword_InputField.text.Equals(SettingsDAO.SettingsContainer.settings.customerPassword))
        {

            if (!newPassword_InputField.text.Equals(string.Empty))
            {
                SettingsDAO.SetPassword(newPassword_InputField.text);
                MessageSystem.ShowDialog("Пароль успешно изменён.");
                Destroy(gameObject);
            }
            else
            {
                MessageSystem.ShowDialog("Введите новый пароль!");
            }

        }
        else
        {
            MessageSystem.ShowDialog("Старый пароль введён неверно!");
        }

    }

    public void Close()
    {
        Destroy(gameObject);
    }
}
