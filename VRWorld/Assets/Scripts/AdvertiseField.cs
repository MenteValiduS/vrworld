﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class AdvertiseField : MonoBehaviour
{
    private VideoPlayer videoPlayer;
    private RawImage rawImage;
    private bool videoStarted;
    private string path;


    private void Update()
    {

        if (videoStarted && rawImage.color.a < 1)
        {
            var newAlphaColor = rawImage.color;
            newAlphaColor.a += Time.deltaTime;
            rawImage.color = newAlphaColor;
        }

        if (Input.anyKeyDown || videoPlayer.isPaused)
        {
            Destroy(gameObject);
        }

    }

    private void Start()
    {
        videoPlayer = gameObject.GetComponent<VideoPlayer>();
        if (System.IO.File.Exists(SettingsDAO.SettingsContainer.settings.adVideoPath))
        {
            videoPlayer.url = SettingsDAO.SettingsContainer.settings.adVideoPath;
            rawImage = gameObject.GetComponent<RawImage>();
            rawImage.texture = videoPlayer.texture;
            videoStarted = false;
            
            StartCoroutine(PlayVideo());
        }
        else
        {
            MessageSystem.ShowDialog("Нет трейлера!\nУкажите путь в настройках или установите показа трейлера в 0 минут.");
        }
    }



    IEnumerator PlayVideo()
    {
        videoPlayer.Prepare();

        if (!videoPlayer.isPrepared)
        {
            yield return new WaitForSeconds(1);
        }

        videoStarted = true;
        rawImage.texture = videoPlayer.texture;
        videoPlayer.SetDirectAudioVolume(0, SettingsDAO.SettingsContainer.settings.volume);
        videoPlayer.Play();
    }
}
