﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ButtonFinder : MonoBehaviour
{
    [SerializeField]
    private Button[] buttons1;
    private static List<Button> buttons;

    public static List<Button> Buttons { get => buttons; private set => buttons = value; }

    private void Awake()
    {
        Debug.Log(Application.persistentDataPath + "/VWDB.db");
        //Buttons = GetButtons();
        //SortButtons();
        Buttons = new List<Button>();
        Buttons.AddRange(buttons1);
        //string message = string.Empty;
        //for (int i = 0; i < Buttons.Count; i++)
        //{
        //    message += i + ": " + buttons[i].name + "\n";
        //}

        //MessageSystem.ShowDialog(message);
    }

    //private void Start()
    //{
    //    string message = string.Empty;
    //    for (int i = 0; i < Buttons.Count; i++)
    //    {
    //        message += i + ": " + buttons[i].name + " x: " + buttons[i].transform.localPosition.x + "y: " + buttons[i].transform.localPosition.y + "\n";
    //    }

    //    MessageSystem.ShowDialog(message);
    //}

    private List<Button> GetButtons()
    {
        List<GameObject> objects = new List<GameObject>();
        objects.AddRange(GameObject.FindGameObjectsWithTag("GameButton"));
        List<Button> buttons = new List<Button>();

        foreach (GameObject gameObject in objects)
        {
            buttons.Add(gameObject.GetComponent<Button>());
        }

        return buttons;
    }

    private void SortButtons()
    {
        //Buttons.OrderBy(x => -(x.transform.localPosition.x)).ThenBy(x => x.transform.localPosition.y).ToList();
//Buttons.OrderBy(x => GetNumber(x.name)).ToList();
    }

    private int GetNumber(string name)
    {
        string number = string.Empty;
        for (int i = 0; i < name.Length; i++)
        {

            if (name[i] == '(')
            {
                i++;
                while (name[i] != ')')
                {
                    number += name[i];
                    i++;
                }

            }

        }

        if (number.Equals(string.Empty))
        {
            return 0;
        }
        else
        {
            return int.Parse(number);
        }
    }
}
