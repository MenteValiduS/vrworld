﻿using UnityEngine;

public class GGChooser : AreaChooser
{
    [SerializeField]
    private GameObject gameArea;
    [SerializeField]
    private GameObject genreArea;
    [SerializeField]
    private GameObject gamesOrGenresArea;


    public void OpenGameArea()
    {
        Instantiate(gameArea, parentArea.transform);
        Destroy(gamesOrGenresArea);
    }

    public void OpenGenreArea()
    {
        Instantiate(genreArea, parentArea.transform);
        Destroy(gamesOrGenresArea);
    }

}
