﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Options : MonoBehaviour
{
    public GameObject spawningArea;
    public GameObject mainMenu;

    private List<GameObject> areas;
    private float rateOfMoving;

    void Start()
    {
        areas = new List<GameObject>();
    }

    public void GoBackToMainMenu()
    {
        SceneManager.LoadScene(1);
    }

    public void PushButton()
    {
        GameObject spawningObject = Instantiate(spawningArea, GameObject.Find("PrefsCanvas").transform);
        spawningObject.GetComponent<Transform>().localPosition += new Vector3(1000f, 0f, 0f);
        GameObject replacingObject = GameObject.FindGameObjectWithTag("OptionArea");
        StartCoroutine(SwitchAreas(replacingObject, spawningObject, new Vector3(-1000f, 0f, 0f)));

        //Destroy(replacingObject);

    }

    public void GoBackButton()
    {
        GameObject spawningObject = Instantiate(mainMenu, GameObject.Find("PrefsCanvas").transform);
        spawningObject.GetComponent<Transform>().localPosition += new Vector3(-1000f, 0f, 0f);
        GameObject replacingObject = GameObject.FindGameObjectWithTag("NewArea");
        StartCoroutine(SwitchAreas(replacingObject, spawningObject, new Vector3(1000f, 0f, 0f)));

        //Destroy(replacingObject);
    }

    private void ChangeGlobalArea()
    {
        areas.AddRange(GameObject.FindGameObjectsWithTag("OptionArea"));

        foreach (GameObject area in areas)
        {
            area.GetComponent<Transform>().position -= new Vector3(100f, 0f, 0f);
        }
    }

    
    private void Movement(GameObject obj, Vector3 dir)
    {
        StartCoroutine(MoveThePlayer(obj, new Vector3(dir.x, dir.y, 0f)));
    }

    public IEnumerator SwitchAreas(GameObject switchedArea, GameObject replacingArea, Vector3 end)
    {
        RectTransform switched = switchedArea.GetComponent<RectTransform>();
        RectTransform replacement = replacingArea.GetComponent<RectTransform>();
        Vector3 coordsForReplacement = new Vector3(switched.localPosition.x, switched.localPosition.y, switched.localPosition.y);

        float dif = Vector3.Distance(end, switched.localPosition);

        

        bool isToRight = true;

        if (end.x < switched.localPosition.x || end.y < switched.localPosition.y)
        {
            isToRight = false;
        }

        Calculations.MaxX = dif;
        Calculations.MaxY = 1000f;

        while (dif > float.Epsilon)
        {
            rateOfMoving = Calculations.GetValueOfSetOfEquation(dif) /1.5f;

            if (dif < 10f)
            {
                Debug.Log("ss");
            }

            if (isToRight)
            {
                rateOfMoving = Mathf.Abs(rateOfMoving);
            }
            else if (rateOfMoving > 0)
            {
                rateOfMoving *= -1f;
            }

            float x = switched.localPosition.x + Time.deltaTime * rateOfMoving;
            switched.localPosition = new Vector3(x, switched.localPosition.y, switched.localPosition.z);

            x = replacement.localPosition.x + Time.deltaTime * rateOfMoving;
            replacement.localPosition = new Vector3(x, replacement.localPosition.y, replacement.localPosition.z);

            //В определённый момент объект проходит мимо точки назначения. Здесь проверяется данная ситуация.
            float val = dif;
            dif = Vector3.Distance(end, switched.localPosition);

            if (dif > val)
                break;

            yield return null;
        }

        switched.localPosition = end;
        replacement.localPosition = coordsForReplacement;
        Destroy(switchedArea);
    }
    

    public IEnumerator MoveThePlayer(GameObject obj, Vector3 end)
    {
        RectTransform rectTransform = obj.GetComponent<RectTransform>();
        float dif = Vector3.Distance(end, rectTransform.localPosition);

        bool isToRight = true;

        if (end.x < rectTransform.localPosition.x || end.y < rectTransform.localPosition.y)
        {
            isToRight = false;
        }

        Calculations.MaxX = dif;
        Calculations.MaxY = 1000f;

        while (dif > float.Epsilon)
        {
            rateOfMoving = Calculations.GetValueOfSetOfEquation(dif) / 3f;

            if (dif < 10f)
            {
                Debug.Log("ss");
            }

            if (isToRight)
            {
                rateOfMoving = Mathf.Abs(rateOfMoving);
            } else if (rateOfMoving > 0)
            {
                rateOfMoving *= -1f;
            }

            float x = rectTransform.localPosition.x + Time.deltaTime * rateOfMoving;
            rectTransform.localPosition = new Vector3(x, rectTransform.localPosition.y, rectTransform.localPosition.z);
            //В определённый момент объект проходит мимо точки назначения. Здесь проверяется данная ситуация.
            float val = dif;
            dif = Vector3.Distance(end, rectTransform.localPosition);

            if (dif > val)
                break;

            yield return null;
        }

        rectTransform.localPosition = end;
    }
}
