﻿using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class SettingsField : MonoBehaviour
{
    [SerializeField]
    private GameObject changePasswordPanel;
    [SerializeField]
    private InputField gameTimeInputField;
    [SerializeField]
    private InputField adTimeInputField;
    [SerializeField]
    private InputField instructionPathInputField;
    [SerializeField]
    private InputField adPathInputField;
    [SerializeField]
    private Slider volumeSlider;
    [SerializeField]
    private InputField browserPathInputField;
    [SerializeField]
    private InputField counterPathInputField;

    private void Start()
    {
        gameTimeInputField.text = SettingsDAO.SettingsContainer.settings.gameTime.ToString();
        adTimeInputField.text = SettingsDAO.SettingsContainer.settings.adTime.ToString();
        instructionPathInputField.text = SettingsDAO.SettingsContainer.settings.instructionImagePath;
        adPathInputField.text = SettingsDAO.SettingsContainer.settings.adVideoPath;
        volumeSlider.value = SettingsDAO.SettingsContainer.settings.volume;
        browserPathInputField.text = SettingsDAO.SettingsContainer.settings.sessionBrowserPath;
        counterPathInputField.text = SettingsDAO.SettingsContainer.settings.counterPath;
    }

    public void Accept()
    {
        MessageSystem.MessageText = string.Empty;

        if (!File.Exists(instructionPathInputField.text) && !instructionPathInputField.text.Equals(string.Empty))
        {
            MessageSystem.MessageText += "Такого файла (инструкция) не существует!\n";
        }

        if (!File.Exists(adPathInputField.text) && !adPathInputField.text.Equals(string.Empty))
        {
            MessageSystem.MessageText += "Такого файла (видео-реклама) не существует!";
        }

        if (!File.Exists(browserPathInputField.text) && !browserPathInputField.text.Equals(string.Empty))
        {
            MessageSystem.MessageText += "Неправильно указан путь к браузеру игр!";
        }

        if (!File.Exists(counterPathInputField.text) && !counterPathInputField.text.Equals(string.Empty))
        {
            MessageSystem.MessageText += "Неправильно указан путь к счётчику!";
        }

        if (!MessageSystem.MessageText.Equals(string.Empty))
        {
            MessageSystem.ShowDialog();
            return;
        }

        GameTimeManager.SetGameTime(int.Parse(gameTimeInputField.text));
        SettingsDAO.Set(int.Parse(gameTimeInputField.text), int.Parse(adTimeInputField.text),
            instructionPathInputField.text, adPathInputField.text, volumeSlider.value, browserPathInputField.text, counterPathInputField.text);
        Close();
    }

    public void ChangePassword()
    {
        Instantiate(changePasswordPanel, gameObject.transform);
    }

    public void Close()
    {
        Destroy(GameObject.Find("CoveringField(Clone)"));
    }
}
