﻿using DB;
using System.Diagnostics;
using System.IO;
using UnityEngine;

public class PlayGame : MonoBehaviour
{
    [SerializeField]
    private GameObject timerField;
    private static bool launched;

    private string args;

    public static bool Launched { get => launched; set => launched = value; }

    private void Start()
    {
        Launched = false;
        
    }

    public void Play()
    {
        if (GameTimeManager.PlayTime != 0)
        {

            string path = ChooseGame.CurrentButton.GetComponent<GameButton>().pathToExe;

            if (File.Exists(path))
            {
                args = GamesDAO.GetDB_IDByName(ChooseGame.CurrentButton.GetComponent<GameButton>().name).ToString() +
                        " " + GameTimeManager.PlayTime.ToString() + " \"" + DataBase.GetDBPath() + "\"";
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.Arguments = args;
                startInfo.FileName = SettingsDAO.SettingsContainer.settings.counterPath;
                startInfo.WorkingDirectory = Path.GetDirectoryName(SettingsDAO.SettingsContainer.settings.counterPath);

                Process process = new Process
                {
                    StartInfo = startInfo
                };
                process.Start();
                process.WaitForExit();

                ////Debug.Log(path);
                //Launched = true;
                //System.Diagnostics.Process.Start(path);
                //GameObject coveringField = Resources.Load<GameObject>("Prefabs/CoveringField");
                //Instantiate(coveringField, GameObject.Find("Canvas").transform);
                //Instantiate(timerField, GameObject.Find("Canvas").transform);
                ////Debug.Log("End IF");
            }
            else
            {
                MessageSystem.MessageText = "Exe-файл не существует!";
                MessageSystem.ShowDialog();
            }

        }
        else
        {
            MessageSystem.ShowDialog("Время игры должно быть больше 0!");
        }
    }

}
