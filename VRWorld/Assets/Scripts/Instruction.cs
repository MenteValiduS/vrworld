﻿using UnityEngine;

public class Instruction : MonoBehaviour
{
    [SerializeField]
    private GameObject instructionField;

    public void Show()
    {
        GameObject coveringField = Resources.Load<GameObject>("Prefabs/CoveringField");
        Instantiate(coveringField, GameObject.Find("Canvas").transform);
        Instantiate(instructionField, GameObject.Find("Canvas").transform);
    }
}
