﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class Timer : MonoBehaviour 
{

	private static double time;
	private static bool isStarted;
    private static bool paused;
    public static bool Paused { get => paused; set => paused = value; }
    public static double Time { get => time; private set => time = value; }

    void Start () {
		Time = 0;
		isStarted = false;
        Paused = false;
	}
	
	void Update () 
	{
        if (!Paused)
        {
            Time += UnityEngine.Time.deltaTime;
        }
	}
		
}
