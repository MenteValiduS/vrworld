﻿using DB;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Opening : MonoBehaviour
{
    [SerializeField]
    private GameObject shutdownpanel;
    [SerializeField]
    private GameObject addingPanel;
    [SerializeField]
    private GameObject settingPanel;
    [SerializeField]
    private GameObject passwordPanel;
    [SerializeField]
    private GameObject optionsPasswordPanel;

	public void ExitBtn()
	{
        GameObject coveringField = Instantiate(Resources.Load<GameObject>("Prefabs/CoveringField"), GameObject.Find("Canvas").transform);
        Instantiate(shutdownpanel, coveringField.transform);
    }

	public void GoToOptions()
	{
        GamesDAO.SaveGames();
        GenresDAO.SaveGenres();
        GameObject coveringField = Instantiate(Resources.Load<GameObject>("Prefabs/CoveringField"), GameObject.Find("Canvas").transform);
        Instantiate(settingPanel, coveringField.transform);
        Instantiate(optionsPasswordPanel, coveringField.transform);
        //SceneManager.LoadScene(2);
    }

    public void ShowAddingPanel()
    {
        GameObject coveringField = Instantiate(Resources.Load<GameObject>("Prefabs/CoveringField"), GameObject.Find("Canvas").transform);
        Instantiate(addingPanel, coveringField.transform);
        Instantiate(passwordPanel, coveringField.transform);
    }

    public void LaunchSessionBrowser()
    {
        var process = new Process
        {
            StartInfo =
              {
                  FileName = SettingsDAO.SettingsContainer.settings.sessionBrowserPath,
                  WorkingDirectory = System.IO.Path.GetDirectoryName(SettingsDAO.SettingsContainer.settings.sessionBrowserPath),
                  Arguments = "\"" + DataBase.GetDBPath() + "\""
              }
        };
        process.Start();
        //System.Diagnostics.Process.Start(SettingsDAO.SettingsContainer.settings.sessionBrowserPath, args);
    }
}
