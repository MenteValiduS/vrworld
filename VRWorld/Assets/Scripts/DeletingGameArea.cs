﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeletingGameArea : AreaChooser
{
    [SerializeField]
    private GameObject lastField;
    [SerializeField]
    private Dropdown gamesDropdown;

    private void Start()
    {
        SetOptions();
    }

    private void SetOptions()
    {
        List<string> options = new List<string>();
        gamesDropdown.ClearOptions();

        foreach (Game game in GamesDAO.Games.games)
        {
            options.Add(game.name);
        }

        gamesDropdown.AddOptions(options);
    }

    public void Delete()
    {
        if (gamesDropdown.options.Count != 0)
        {

            if (gamesDropdown.value >= gamesDropdown.options.Count)
            {
                gamesDropdown.value = gamesDropdown.options.Count - 1;
            }

            if (!gamesDropdown.options[gamesDropdown.value].text.Equals(string.Empty))
            {
                GamesDAO.Delete(gamesDropdown.options[gamesDropdown.value].text);
                GamesDAO.SaveGames();
                GamePanel.IsToUpdate = true;
                SetOptions();
                MessageSystem.MessageText = "Игра успешно удалена!";
                MessageSystem.ShowDialog();
            }

        }
        else
        {
            MessageSystem.MessageText = "Список игр пуст!";
            MessageSystem.ShowDialog();
        }

    }

    public void Back()
    {
        Destroy(gameObject);
        GameObject editingPanelObject = GameObject.Find("EditingPanel(Clone)");
        EditingPanel editingPanel = editingPanelObject.GetComponent<EditingPanel>();
        editingPanel.SetSmallWindow();
        Instantiate(lastField, editingPanelObject.transform);
    }
}
