﻿using System.IO;
using UnityEngine;

public class Trailer : MonoBehaviour
{
    [SerializeField]
    private GameObject trailerField;

    public void Show()
    {

        if (File.Exists(ChooseGame.CurrentButton.GetComponent<GameButton>().pathToTrailer))
        {
            Instantiate(trailerField, GameObject.Find("Canvas").transform);
        }
        else
        {
            MessageSystem.MessageText = "Видео не существует в указанной папке!";
            MessageSystem.ShowDialog();
        }

    }
}
