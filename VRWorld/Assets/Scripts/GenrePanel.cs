﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenrePanel : MonoBehaviour
{
    private static Text descriptionText;

    public static Text GenreNameText { get; private set; }
    public static bool IsToUpdate { get; set; }


    void Start()
    {
        IsToUpdate = false;
        descriptionText = GameObject.Find("Description").GetComponent<Text>();
        GenreNameText = GameObject.Find("GenreText").GetComponent<Text>();
        GenresDAO.LoadGenres();
        SetGenre();
    }

    private void Update()
    {
        if (IsToUpdate)
        {
            SetGenre();
            IsToUpdate = false;
            GamePanel.IsToUpdate = true;
        }
    }

    private void SetGenre()
    {
        GenreNameText.text = "Все";
        descriptionText.text = "Описание:\nВ данном разделе отображаются все игры, добавленные в приложение.";
    }

    public void GetNameOfLeftGenre()
    {
        List<Genre> genres = GenresDAO.Genres.genres;

        if (0 == genres.Count)
        {
            return;
        } else
        {

            if (GenreNameText.text.Equals("Все"))
            {

                if (1 == genres.Count)
                {
                    GenreNameText.text = genres[0].name;
                    descriptionText.text = "Описание:\n" + genres[0].description;
                } else
                {
                    int newIndex = genres.Count - 1;
                    GenreNameText.text = genres[newIndex].name;
                    descriptionText.text = "Описание:\n" + genres[newIndex].description;
                }

            } else
            {

                if (1 == genres.Count)
                {
                    GenreNameText.text = "Все";
                    descriptionText.text = "Описание:\nВ данном разделе отображаются все игры, добавленные в приложение.";
                }
                else
                {
                    int newIndex = genres.FindIndex(x => x.name == GenreNameText.text) - 1;

                    if (newIndex < 0)
                    {
                        GenreNameText.text = "Все";
                        descriptionText.text = "Описание:\nВ данном разделе отображаются все игры, добавленные в приложение.";
                    }
                    else
                    {
                        GenreNameText.text = genres[newIndex].name;
                        descriptionText.text = "Описание:\n" + genres[newIndex].description;
                    }

                }


            }
            
        }

        GamePanel.IsToUpdate = true;
    }

    public void GetNameOfRightGenre()
    {
        List<Genre> genres = GenresDAO.Genres.genres;

        if (0 == genres.Count)
        {
            return;
        } else
        {
            if (GenreNameText.text.Equals("Все"))
            {
                GenreNameText.text = genres[0].name;
                descriptionText.text = "Описание:\n" + genres[0].description;
            }
            else
            {
                if (1 == genres.Count)
                {
                    GenreNameText.text = "Все";
                    descriptionText.text = "Описание:\nВ данном разделе отображаются все игры, добавленные в приложение.";
                }
                else
                {
                    int newIndex = genres.FindIndex(x => x.name == GenreNameText.text) + 1;

                    if (newIndex > (genres.Count - 1))
                    {
                        GenreNameText.text = "Все";
                        descriptionText.text = "Описание:\nВ данном разделе отображаются все игры, добавленные в приложение.";
                    }
                    else
                    {
                        GenreNameText.text = genres[newIndex].name;
                        descriptionText.text = genres[newIndex].description;
                    }

                }

            }

        }

        GamePanel.IsToUpdate = true;
    }
    
}
