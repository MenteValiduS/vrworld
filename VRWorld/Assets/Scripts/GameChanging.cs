﻿using UnityEngine;

public class GameChanging : AreaChooser
{
    [SerializeField]
    private GameObject lastField;
    [SerializeField]
    private GameObject addingGameArea;
    [SerializeField]
    private GameObject editingGameArea;
    [SerializeField]
    private GameObject deletingGameArea;

    public void AddGame()
    {
        EditingPanel editingPanel = parentArea.GetComponent<EditingPanel>();
        editingPanel.SetBigWindow();
        Instantiate(addingGameArea, parentArea.transform);
        CloseLastArea();
    }

    public void EditGame()
    {
        EditingPanel editingPanel = parentArea.GetComponent<EditingPanel>();
        editingPanel.SetBigWindow();
        Instantiate(editingGameArea, parentArea.transform);
        CloseLastArea();
    }

    public void DeleteGame()
    {
        EditingPanel editingPanel = parentArea.GetComponent<EditingPanel>();
        editingPanel.SetDeletingWindow();
        Instantiate(deletingGameArea, parentArea.transform);
        CloseLastArea();
    }

    public void Back()
    {
        Destroy(gameObject);
        Instantiate(lastField, parentArea.transform);
    }

}
