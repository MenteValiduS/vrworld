﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class Player : MonoBehaviour
{
    private VideoPlayer sourceVideoPlayer;
    private RawImage rawImage;
    private bool videoStarted;

    private void Update()
    {

        if (videoStarted && rawImage.color.a < 1)
        {
            var newAlphaColor = rawImage.color;
            newAlphaColor.a += Time.deltaTime;
            rawImage.color = newAlphaColor;
        }


    }


    public void Play(string path, VideoPlayer videoPlayer, RawImage image)
    {
        sourceVideoPlayer = videoPlayer;
        sourceVideoPlayer.url = path;
        rawImage = image;
        rawImage.texture = sourceVideoPlayer.texture;
        videoStarted = false;
        StartCoroutine(PlayTrailer());
    }



    IEnumerator PlayTrailer()
    {
        sourceVideoPlayer.Prepare();

        if (!sourceVideoPlayer.isPrepared)
        {
            yield return new WaitForSeconds(1);
        }

        videoStarted = true;
        rawImage.texture = sourceVideoPlayer.texture;
        sourceVideoPlayer.Play();
    }


    public void PlayLargeVideo()
    {

    }


}
