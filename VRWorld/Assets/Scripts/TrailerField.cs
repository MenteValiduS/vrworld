﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class TrailerField : MonoBehaviour
{

    private VideoPlayer videoPlayer;
    private RawImage rawImage;
    private bool videoStarted;

    private void Update()
    {
//Debug.Log("0" + videoPlayer.GetDirectAudioVolume(0));
        //Debug.Log("1" + videoPlayer.GetDirectAudioVolume(1));
        if (videoStarted && rawImage.color.a < 1)
        {
            var newAlphaColor = rawImage.color;
            newAlphaColor.a += Time.deltaTime;
            rawImage.color = newAlphaColor;
        }

        if (Input.anyKeyDown || videoPlayer.isPaused)
        {
            Destroy(gameObject);
        }

    }

    private void Start()
    {
        videoPlayer = gameObject.GetComponent<VideoPlayer>();
        videoPlayer.url = ChooseGame.CurrentButton.GetComponent<GameButton>().pathToTrailer;
        rawImage = gameObject.GetComponent<RawImage>();
        rawImage.texture = videoPlayer.texture;
        videoStarted = false;
        StartCoroutine(PlayTrailer());
    }

    

    IEnumerator PlayTrailer()
    {
        videoPlayer.Prepare();

        if (!videoPlayer.isPrepared)
        {
            yield return new WaitForSeconds(1);
        }

        videoStarted = true;
        rawImage.texture = videoPlayer.texture;
        videoPlayer.SetDirectAudioVolume(0, SettingsDAO.SettingsContainer.settings.volume);
        //Debug.Log(SettingsDAO.SettingsContainer.settings.volume);
        //Debug.Log("0" + videoPlayer.GetDirectAudioVolume(0));
        //Debug.Log("1" + videoPlayer.GetDirectAudioVolume(1));
        videoPlayer.Play();
        //videoPlayer.SetDirectAudioVolume(1, SettingsDAO.SettingsContainer.settings.volume);
    }
}
