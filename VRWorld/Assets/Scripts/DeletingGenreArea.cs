﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeletingGenreArea : AreaChooser
{
    [SerializeField]
    private GameObject lastField;
    [SerializeField]
    private Dropdown genresDropdown;

    private void Start()
    {
        SetOptions();
    }

    private void SetOptions()
    {
        List<string> options = new List<string>();
        genresDropdown.ClearOptions();

        foreach (Genre genre in GenresDAO.Genres.genres)
        {
            options.Add(genre.name);
        }

        genresDropdown.AddOptions(options);
    }

    public void Delete()
    {

        if (0 != genresDropdown.options.Count)
        {
            if (genresDropdown.value >= genresDropdown.options.Count)
            {
                genresDropdown.value = genresDropdown.options.Count - 1;
            }

            GenresDAO.Delete(genresDropdown.options[genresDropdown.value].text);
            GenresDAO.SaveGenres();
            SetOptions();
            GenrePanel.IsToUpdate = true;
            MessageSystem.MessageText = "Жанр успешно удален!";
            MessageSystem.ShowDialog();
        }
        else
        {
            MessageSystem.MessageText = "Список жанров пуст!";
            MessageSystem.ShowDialog();
        }

    }

    public new void Close()
    {
        //Destroy(GameObject.Find("EditingPanel(Clone)"));
        Destroy(GameObject.Find("CoveringField(Clone)"));
    }

    public void Back()
    {
        Destroy(gameObject);
        GameObject editingPanelObject = GameObject.Find("EditingPanel(Clone)");
        EditingPanel editingPanel = editingPanelObject.GetComponent<EditingPanel>();
        editingPanel.SetSmallWindow();
        Instantiate(lastField, editingPanelObject.transform);
    }
}
