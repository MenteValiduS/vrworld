﻿using System.IO;
using UnityEngine;

public class JsonDAO
{
    public static string JsonPath { private get; set; }
    

    private static string GetPath()
    {
        return Path.Combine(Application.dataPath, JsonPath);
    }

    public static void Save<T>(T obj)
    {
        string jsonStr = JsonUtility.ToJson(obj);
        File.WriteAllText(GetPath(), jsonStr);
    }

    public static T Load<T>()
    {
        if (File.Exists(GetPath()))
            return JsonUtility.FromJson<T>(File.ReadAllText(GetPath()));
        else return default(T);
    }
}
