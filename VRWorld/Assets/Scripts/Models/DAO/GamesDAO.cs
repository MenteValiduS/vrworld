﻿using DB;
using System.Collections.Generic;

public class GamesDAO
{
    private static GameData games;

    public static GameData Games
    {
        get
        {

            if (games == null)
            {
                return games = new GameData();
            }
            else
            {
                return games;
            }

        }

        private set
        {
            games = value;
        }
    }

    public static void AddGame(string name, string pathToExe, string pathToImage, string pathToVideo, string genre, string description)
    {
        Game game = new Game(name, pathToImage, pathToExe, genre, pathToVideo, description);
        DataBase.Insert(game);
        game.ID = DataBase.GetGameID(game.name);
        Games.games.Add(game);
        //SaveGames();
    }

    public static void Delete(string name)
    {
        DataBase.DeleteGame(name);
        Games.games.RemoveAll(x => x.name == name);
        //SaveGames();
    }

    public static void SaveGames()
    {
        //JsonDAO.JsonPath = "games.json";
        //JsonDAO.Save(Games);
    }
	
    public static void LoadGames()
    {
        //JsonDAO.JsonPath = "games.json";
        //Games = JsonDAO.Load<GameData>();
        Games = DataBase.SelectGames();
    }

    public static int GetGamesCount(string genre)
    {
        //Debug.Log("Метод GetGamesCount вернул: " + Games.games.FindAll(x => x.genre.Equals(genre)).Count);

        if (genre.Equals("Все"))
        {
            return Games.games.Count;
        }
        else
        {
            return Games.games.FindAll(x => x.genre.Equals(genre)).Count;
        }
        
    }

    public static List<Game> GetGames()
    {
        return Games.games;
    }

    public static List<Game> GetGames(string genre, int offset, int numberOfGames)
    {
        List<Game> gGames;
        List<Game> nGames = new List<Game>(); ;

        if (genre.Equals("Все"))
        {
            gGames = Games.games;
        }
        else
        {
            gGames = Games.games.FindAll(x => x.genre.Equals(genre));
        }

        for (int i = offset; (i < offset + numberOfGames) && i < gGames.Count; i++)
        {
            nGames.Add(gGames[i]);
        }

        return nGames;
    }

    public static int GetIDByName(string name)
    {
        for (int i = 0; i < Games.games.Count; i++)
        {
            if (Games.games[i].name.Equals(name))
                return i;
        }

        return -1;
    }

    public static int GetDB_IDByName(string name)
    {
        for (int i = 0; i < Games.games.Count; i++)
        {
            if (Games.games[i].name.Equals(name))
                return Games.games[i].ID;
        }

        return -1;
    }

    public static void ChangeGameData(int i, Game game)
    {
        game.ID = Games.games[i].ID;
        DataBase.UpdateGame(game);
        Games.games[i] = game;
    }

    public static void DropGenre(string genreName)
    {

        foreach (Game game in Games.games)
        {

            if (game.genre.Equals(genreName))
            {
                game.genre = "Все";
            }

        }

    }
}
