﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsDAO
{
    private static SettingsContainer settingsContainer;

    public static SettingsContainer SettingsContainer
    {
        get
        {

            if (settingsContainer == null)
            {
                return settingsContainer = new SettingsContainer();
            }
            else
            {
                return settingsContainer;
            }

        }

        private set
        {
            settingsContainer = value;
        }
    }

    public static void Set(int gTime, int adTime, string instrPath, string adPath, float volume, string browserPath, string counterPath)
    {
        SettingsContainer.settings.gameTime = gTime;
        SettingsContainer.settings.adTime = adTime;
        SettingsContainer.settings.instructionImagePath = instrPath;
        SettingsContainer.settings.adVideoPath = adPath;
        SettingsContainer.settings.volume = volume;
        SettingsContainer.settings.sessionBrowserPath = browserPath;
        SettingsContainer.settings.counterPath = counterPath;

        Save();
    }

    public static void SetPassword(string password)
    {
        SettingsContainer.settings.customerPassword = password;
    }

    private static void Save()
    {
        JsonDAO.JsonPath = "settings.json";
        JsonDAO.Save(SettingsContainer);
    }

    public static void LoadSettings()
    {
        JsonDAO.JsonPath = "settings.json";
        SettingsContainer = JsonDAO.Load<SettingsContainer>();
    }
}
