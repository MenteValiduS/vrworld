﻿using DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Models.DAO
{
    class SessionDAO
    {
        private static List<Session> sessions;

        public static List<Session> Sessions
        {
            get
            {

                if (sessions == null)
                {
                    return sessions = new List<Session>();
                }
                else
                {
                    return sessions;
                }

            }

            private set => sessions = value;
        }

        public static void Add(string gameName, int gameTime, string date, string additionalInfo)
        {
            Session session = new Session();
            session.game = gameName;
            session.gameTime = gameTime;
            session.date = date;
            session.additionalInfo = additionalInfo;

            Sessions.Add(session);

            DataBase.Insert(session);
            
            //ДОБАВЛЕНИЕ В БД*************!!!!
        }
    }
}
