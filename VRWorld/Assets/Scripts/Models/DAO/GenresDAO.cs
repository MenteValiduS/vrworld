﻿using DB;

public class GenresDAO
{
    private static GenreContainer genres;

    public static GenreContainer Genres
    {
        get
        {
            if (genres == null)
            {
                return genres = new GenreContainer();
            }
            else return genres;
        }

        private set => genres = value; }

    public static void LoadGenres()
    {
        //JsonDAO.JsonPath = "genres.json";
        //Genres = JsonDAO.Load<GenreContainer>();
        Genres = DataBase.SelectGenres();
    }

    public static void SaveGenres()
    {
        JsonDAO.JsonPath = "genres.json";
        JsonDAO.Save(genres);
    }

    public static void Add(Genre genre)
    {
        DataBase.Insert(genre);
        genre.ID = DataBase.GetGenreID(genre.name);
        Genres.genres.Add(genre);
        //SaveGenres();
    }

    public static void Delete(string name)
    {
        DataBase.DeleteGenre(name);
        Genres.genres.RemoveAll(x => x.name == name);
        GamesDAO.DropGenre(name);
        //SaveGenres();
    }

    public static bool Exists(string name)
    {

        if (Genres.genres.FindAll(x => x.name.Equals(name)).Count != 0)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

}
