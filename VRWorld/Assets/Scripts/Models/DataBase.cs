﻿using System.Collections;
using System.Collections.Generic;
using Mono.Data.Sqlite;
using System.Data;
using UnityEngine;
using Assets.Scripts.Models;
using System.Text;
using System;

namespace DB
{
    public class DataBase
    {
        private static List<Session> sessionListBuf;
        private static Session sessionBuf;
        private static StringBuilder querryBuilder;

        private static string GetConnectionString()
        {
            return "URI=file:" + System.IO.Path.Combine(Application.dataPath, "VWDB.db");
        }


        public static string GetDBPath()
        {
            return System.IO.Path.Combine(Application.dataPath, "VWDB.db");
        }


        ////////////////////////////////////////////////////////////////////////////////////// INSERT SECTION
        public static void Insert(Genre genre)
        {

            try
            {
                SqliteConnection connection = new SqliteConnection(GetConnectionString());
                connection.Open();

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "INSERT INTO Genres (name, description)" +
                        "VALUES (@name, @descr)";

                    cmd.Parameters.Add(new SqliteParameter("name", genre.name));
                    cmd.Parameters.Add(new SqliteParameter("descr", genre.description));

                    var result = cmd.ExecuteNonQuery();
                    Debug.Log(result);
                }

                connection.Close();

            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();

            }
            

        }

        public static int GetGenreID(string name)
        {
            int ID = -1;


            try
            {
                SqliteConnection connection = new SqliteConnection(GetConnectionString());
                SqliteConnection.ClearAllPools();
                connection.Open();

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    // cmd.CommandText = "SELECT ID " +
                      //  "FROM Genres" +
                        //"WHERE name=@name";

                    cmd.CommandText = "SELECT ID, name " +
                        "FROM Genres";

                    //cmd.Parameters.Add(new SqliteParameter("name", name));

                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {

                            if (reader.GetString(1).Equals(name))
                            {
                                ID = reader.GetInt32(0);
                            }

                        }

                    }

                }

                connection.Close();
                return ID;
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        public static bool GenreInsertCheck(string name)
        {
            try
            {
                bool res = false;

                SqliteConnection connection = new SqliteConnection(GetConnectionString());
                connection.Open();

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT ID, name FROM Genres ";// +
                        //"WHERE name=@name";
                    //cmd.Parameters.Add(new SqliteParameter("name", name));

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (!reader.Read() && reader.GetString(1).Equals(name))
                        {
                            res = true;
                        }
                    }
                }

                connection.Close();
                return res;
            }
            catch (SqliteException ex)
            {
                Debug.Log(new StringBuilder("Error with a genre (" + name + "):\n" + ex.Message));
                return false;
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        public static void Insert(Game game)
        {
            int genreID = 1;

            if (querryBuilder == null)
            {
                querryBuilder = new StringBuilder();
            }
            else
            {
                querryBuilder.Clear();
            }
            
            try
            {
                SqliteConnection connection = new SqliteConnection(GetConnectionString());
                connection.Open();
                querryBuilder.Append("SELECT ID " +
                        "FROM Genres " +
                        "WHERE name=@name LIMIT 1");

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = querryBuilder.ToString();

                    cmd.Parameters.Add(new SqliteParameter("name", game.genre));

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            genreID = reader.GetInt32(0);
                        }
                    }

                    
                }



                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "INSERT INTO Games " +
                        "(name, genre, description, exe_path, image_path, trailer_path)" +
                        "VALUES " +
                        "(@name, @genre, @description, @exe_path, @image_path, @trailer_path)";

                    cmd.Parameters.Add(new SqliteParameter("name", game.name));
                    cmd.Parameters.Add(new SqliteParameter("genre", genreID));
                    cmd.Parameters.Add(new SqliteParameter("description", game.description));
                    cmd.Parameters.Add(new SqliteParameter("exe_path", game.pathToExe));
                    cmd.Parameters.Add(new SqliteParameter("image_path", game.pathToImage));
                    cmd.Parameters.Add(new SqliteParameter("trailer_path", game.pathToTrailer));

                    var result = cmd.ExecuteNonQuery();
                    Debug.Log(result);
                }

                connection.Close();
            }
            catch (SqliteException ex)
            {
                Debug.Log(ex.Message);
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }

        }

        public static int GetGameID(string name)
        {
            try
            {
                int ID = -1;

                SqliteConnection connection = new SqliteConnection(GetConnectionString());
                connection.Open();

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT ID, name FROM Games";// +
                        //"WHERE name=@name";

                    cmd.Parameters.Add(new SqliteParameter("name", name));

                    using (var reader = cmd.ExecuteReader()) 
                    {

                        while (reader.Read())
                        {

                            if (reader.GetString(1).Equals(name))
                            {
                                ID = reader.GetInt32(0);
                            }

                        }

                    }

                }

                connection.Close();
                return ID;
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        public static bool GameInsertCheck(string name)
        {
            try
            {
                bool res = false;

                SqliteConnection connection = new SqliteConnection(GetConnectionString());
                connection.Open();

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT ID FROM Games " +
                        "WHERE name=@name LIMIT 1";
                    cmd.Parameters.Add(new SqliteParameter("name", name));

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            res = false;
                        }
                        else
                        {
                            res = true;
                        }
                    }
                }

                connection.Close();
                return res;
            }
            catch (SqliteException ex)
            {
                Debug.Log(new StringBuilder("Error with a game ("+name+"):\n"+ex.Message));
                return false;
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        public static void Insert(Session session)
        {
            try
            {
                SqliteConnection connection = new SqliteConnection(GetConnectionString());
                connection.Open();

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "INSERT INTO Sessions " +
                        "(game, play_time, date, additional)" +
                        "VALUES " +
                        "(@game, @play_time, @date, @additional)";

                    cmd.Parameters.Add(new SqliteParameter("game", session.game));
                    cmd.Parameters.Add(new SqliteParameter("play_time", session.gameTime));
                    cmd.Parameters.Add(new SqliteParameter("date", session.date));
                    cmd.Parameters.Add(new SqliteParameter("additional", session.additionalInfo));

                    var result = cmd.ExecuteNonQuery();
                    Debug.Log(result);
                }

                connection.Close();
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }

        }
        ////////////////////////////////////////////////////////////////////////////////////// SELECT SECTION

        public static GameData SelectGames()
        {
            try
            {
                GameData gameData = new GameData();
                SqliteConnection connection = new SqliteConnection(GetConnectionString());
                connection.Open();

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "select Ga.name, Ge.name, Ga.description, Ga.exe_path, Ga.image_path, Ga.trailer_path, Ga.ID " +
                        "from Games Ga inner join Genres Ge " +
                        "on Ga.genre = Ge.ID";

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Game gameBuf = new Game();
                            gameBuf.name = reader.GetString(0);
                            gameBuf.genre = reader.GetString(1);
                            gameBuf.description = reader.GetString(2);
                            gameBuf.pathToExe = reader.GetString(3);
                            gameBuf.pathToImage = reader.GetString(4);
                            gameBuf.pathToTrailer = reader.GetString(5);
                            gameBuf.ID = reader.GetInt32(6);
                            gameData.games.Add(gameBuf);
                        }
                    }

                }

                connection.Close();
                return gameData;
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        public static GenreContainer SelectGenres()
        {
            GenreContainer genreContainer = new GenreContainer();

            try
            {
                SqliteConnection connection = new SqliteConnection(GetConnectionString());
                connection.Open();

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "select ID, name, description " +
                        "from Genres WHERE NOT name='Все'";

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Genre genreBuf = new Genre();
                            genreBuf.ID = reader.GetInt32(0);
                            genreBuf.name = reader.GetString(1);
                            genreBuf.description = reader.GetString(2);
                            genreContainer.genres.Add(genreBuf);
                        }
                    }

                }

                connection.Close();
                return genreContainer;
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        public static List<Session> SelectSessions()
        {
            sessionListBuf.Clear();

            try
            {
                SqliteConnection connection = new SqliteConnection(GetConnectionString());
                connection.Open();

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "select Ga.name, Ge.name, S.play_time, S.[date], S.additional " +
                        "from Sessions S inner join Games Ga " +
                        "on S.game = Ga.ID inner join Genres Ge " +
                        "on Ga.genre = Ge.ID";

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            sessionBuf.game = reader.GetString(0);
                            sessionBuf.genre = reader.GetString(1);
                            sessionBuf.gameTime = reader.GetInt32(2);
                            sessionBuf.date = reader.GetDateTime(3).ToString();
                            sessionBuf.additionalInfo = reader.GetString(4);
                            sessionListBuf.Add(sessionBuf);
                        }
                    }

                }

                connection.Close();
                return sessionListBuf;
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////// DELETE SECTION
        public static void DeleteGame(string gameName)
        {
            try
            {
                SqliteConnection connection = new SqliteConnection(GetConnectionString());
                connection.Open();

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "DELETE FROM Games WHERE name=@name";

                    cmd.Parameters.Add(new SqliteParameter("name", gameName));

                    var result = cmd.ExecuteNonQuery();
                    Debug.Log(result);
                }

                connection.Close();
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        public static void DeleteGenre(string genreName)
        {
            //using (var connection = new SqliteConnection(GetConnectionString()))
            //{
            try
            {
                SqliteConnection connection = new SqliteConnection(GetConnectionString());
                connection.Open();

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "DELETE FROM Genres WHERE name=@name";

                    cmd.Parameters.Add(new SqliteParameter("name", genreName));

                    var result = cmd.ExecuteNonQuery();
                    Debug.Log(result);
                }

                connection.Close();
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////// UPDATE SECTION
        public static void UpdateGame(Game game)
        {
            int genreID = -1;

            try
            {
                SqliteConnection connection = new SqliteConnection(GetConnectionString());
                connection.Open();

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT ID FROM Genres WHERE name=@name";
                    cmd.Parameters.Add(new SqliteParameter("name", game.genre));

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            genreID = reader.GetInt32(0);
                        }
                    }
                }

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "UPDATE Games SET name=@name, genre=@genre, description=@description, " +
                        "exe_path=@exe, image_path=@image, trailer_path=@trailer WHERE ID=@id";

                    cmd.Parameters.Add(new SqliteParameter("name", game.name));
                    cmd.Parameters.Add(new SqliteParameter("genre", genreID));
                    cmd.Parameters.Add(new SqliteParameter("description", game.description));
                    cmd.Parameters.Add(new SqliteParameter("exe", game.pathToExe));
                    cmd.Parameters.Add(new SqliteParameter("image", game.pathToImage));
                    cmd.Parameters.Add(new SqliteParameter("trailer", game.pathToTrailer));
                    cmd.Parameters.Add(new SqliteParameter("id", game.ID));

                    var result = cmd.ExecuteNonQuery();
                    Debug.Log(result);
                }

                connection.Close();
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        public static void UpdateGenre(Genre genre)
        {
            try
            {
                SqliteConnection connection = new SqliteConnection(GetConnectionString());
                connection.Open();

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "UPDATE Genres SET name=@name, description=@description WHERE ID=@id";

                    cmd.Parameters.Add(new SqliteParameter("name", genre.name));
                    cmd.Parameters.Add(new SqliteParameter("description", genre.description));
                    cmd.Parameters.Add(new SqliteParameter("id", genre.ID));

                    var result = cmd.ExecuteNonQuery();
                    Debug.Log(result);
                }

                connection.Close();
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////// CREATE SECTION
        public static void CreateDatabase()
        {

        }
        // CREATE TABLE "Games" ( `ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `name` TEXT NOT NULL UNIQUE, `genre` INTEGER NOT NULL DEFAULT 1, `description` TEXT, `exe_path` TEXT NOT NULL, `image_path` TEXT NOT NULL, `trailer_path` TEXT NOT NULL, FOREIGN KEY(`genre`) REFERENCES `Genres`(`ID`) )

        private static void CreateGamesTable()
        {
            using (var connection = new SqliteConnection(GetConnectionString()))
            {
                connection.Open();

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "CREATE TABLE \"Games\" " +
                        "( `ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                        "`name` TEXT NOT NULL UNIQUE, `genre` INTEGER NOT NULL DEFAULT 1, " +
                        "`description` TEXT, `exe_path` TEXT NOT NULL, `image_path` TEXT NOT NULL, " +
                        "`trailer_path` TEXT NOT NULL, " +
                        "FOREIGN KEY(`genre`) REFERENCES `Genres`(`ID`) on delete set default )";
                    cmd.ExecuteNonQuery();
                }

                connection.Close();
            }
        }


        ////////////////////////////////////////////////////////////////////////////////////// CREATE SECTION
        // CREATE TABLE `Genres` ( `ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `name` TEXT NOT NULL UNIQUE, `description` TEXT )

        ////////////////////////////////////////////////////////////////////////////////////// CREATE SECTION
        // CREATE TABLE `Sessions` ( `ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `game` INTEGER NOT NULL, `play_time` INTEGER NOT NULL CHECK(play_time >= 0), `date` TEXT NOT NULL, `additional` TEXT )

    }

}
