﻿[System.Serializable]
public class Settings
{
    public int gameTime;
    public string instructionImagePath;
    public int adTime;
    public string adVideoPath;
    public string customerPassword;
    public float volume;
    public string sessionBrowserPath;
    public string counterPath;
}
