﻿using UnityEngine;

public class GameButton : MonoBehaviour
{
    public new string name;
    public string genre;
    public string pathToExe;
    public string pathToImage;
    public string pathToTrailer;
    public string description;
	
}
