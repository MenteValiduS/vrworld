﻿[System.Serializable]
public class Genre
{
    public int ID;
    public string name;
    public string description;

    public Genre(string name, string description)
    {
        this.name = name;
        this.description = description;
    }

    public Genre()
    {
        this.name = string.Empty;
        this.description = string.Empty;
    }
}
