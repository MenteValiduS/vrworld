﻿namespace Assets.Scripts.Models
{
    public class Session
    {
        public string game;
        public string genre;
        public int gameTime;
        public string date;
        public string additionalInfo;
    }
}
