﻿using System.Collections.Generic;

[System.Serializable]
public class GenreContainer
{
    public List<Genre> genres = new List<Genre>();
}
