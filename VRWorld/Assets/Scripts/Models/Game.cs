﻿[System.Serializable] //Для хранения в JSON
public class Game
{
    public int ID;
    public string name;
    public string pathToImage;
    public string pathToExe;
    public string genre;
    public string description;
    public string pathToTrailer;

    public Game(string name, string pathToImage, string pathToExe, string genre, string pathToTrailer, string description = "Отсутствует")
    {
        this.name = name;
        this.pathToImage = pathToImage;
        this.pathToExe = pathToExe;
        this.genre = genre;
        this.pathToTrailer = pathToTrailer;
        this.description = description;
    }

    public Game()
    {
        this.name = string.Empty;
        this.pathToImage = string.Empty;
        this.pathToExe = string.Empty;
        this.genre = string.Empty;
        this.pathToTrailer = string.Empty;
        this.description = string.Empty;
    }
}
