﻿using UnityEngine;
using UnityEngine.UI;

public class ChooseGame : MonoBehaviour {

    private static Button currentButton;

    public static Button CurrentButton
    {
        get
        {
            return currentButton;
        }

        private set
        {
            currentButton = value;
        }
    }

    public void SetGame()
    {
        CurrentButton = transform.gameObject.GetComponent<Button>();
        SetUIEnvironment();
    }

    private Button playGameButton;
    private Button playTrailerButton;

    private void Start()
    {
        playGameButton = GameObject.Find("PlayButton").GetComponent<Button>();
        playTrailerButton = GameObject.Find("PlayVideoButton").GetComponent<Button>();
        playGameButton.enabled = false;
        playTrailerButton.enabled = false;
    }

    private static void SetUIEnvironment()
    {
        Text description = GameObject.Find("Description").GetComponent<Text>();
        GameButton curGame = CurrentButton.GetComponent<GameButton>();
        description.text = curGame.name + ":\n" + curGame.description;
        Button playGameButton = GameObject.Find("PlayButton").GetComponent<Button>();
        Button playTrailerButton = GameObject.Find("PlayVideoButton").GetComponent<Button>();

        if (curGame.name.Equals(string.Empty) && playGameButton.enabled && playTrailerButton.enabled)
        {
            playGameButton.enabled = false;
            playTrailerButton.enabled = false;

        } else if (!curGame.name.Equals(string.Empty) && !playGameButton.enabled && !playTrailerButton.enabled)
        {
            playGameButton.enabled = true;
            playTrailerButton.enabled = true;
        }
    }
}
