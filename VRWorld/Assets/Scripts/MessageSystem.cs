﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageSystem : MonoBehaviour
{
    [SerializeField]
    private GameObject messagePanel;

    private static string messageText;
    private static bool isToShow;
    private GameObject coveringField;
    

    public static string MessageText { get => messageText; set => messageText = value; }
    public static bool IsToShow { private get => isToShow; set => isToShow = value; }

    private void Start()
    {
        messageText = string.Empty;
        IsToShow = false;
    }

    private void Update()
    {
        if (!IsToShow && Input.anyKey && coveringField != null)
        {
            Destroy(coveringField);
        }

        if (IsToShow)
        {
            coveringField = Instantiate(Resources.Load<GameObject>("Prefabs/CoveringField"), transform);
            Instantiate(messagePanel, coveringField.transform);
            Text text = GameObject.Find("MessageText").GetComponent<Text>();
            text.text = MessageText;
            IsToShow = false;
        }

    }

    public static void ShowDialog()
    {
        IsToShow = true;
    }

    public static void ShowDialog(string message)
    {
        MessageText = message;
        IsToShow = true;
    }


}
