﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class ShutdownField : MonoBehaviour
{
    [SerializeField]
    private GameObject lockPanel;

    [DllImport("user32.dll")]
    private static extern bool ShowWindow(IntPtr hwnd, int nCmdShow);
    [DllImport("user32.dll")]
    private static extern IntPtr GetActiveWindow();

    void Update()
    {
        HideIfClickedOutside();
    }

    public void Minimize()
    {
        ShowWindow(GetActiveWindow(), 2);
    }

    public void Lock()
    {
        Instantiate(lockPanel, GameObject.Find("CoveringField(Clone)").transform);
        Destroy(gameObject);
    }

    public void Restart()
    {
        System.Diagnostics.Process.Start("shutdown.exe", "-r -t 0");
    }

    public void Shutdown()
    {
        System.Diagnostics.Process.Start("shutdown.exe", "-s -t 0");
    }

    private void HideIfClickedOutside()
    {
        if ((Input.anyKey && !Input.GetMouseButton(0)) ||
            Input.GetMouseButton(0) && gameObject.activeSelf &&
            !RectTransformUtility.RectangleContainsScreenPoint(
                gameObject.GetComponent<RectTransform>(),
                Input.mousePosition,
                Camera.main))
        {
            Destroy(GameObject.Find("CoveringField(Clone)"));

        }
    }
}
