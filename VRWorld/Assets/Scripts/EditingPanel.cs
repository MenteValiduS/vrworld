﻿using UnityEngine;

public class EditingPanel : MonoBehaviour
{
    [SerializeField]
    private GameObject gamesOrGenresArea;

    private void Start()
    {
        SetSmallWindow();
        Instantiate(gamesOrGenresArea, transform);
    }

    public void SetSmallWindow()
    {
        RectTransform editingPanelRectTransform = gameObject.GetComponent<RectTransform>();
        editingPanelRectTransform.offsetMax = new Vector2(-600f,-300f);
        editingPanelRectTransform.offsetMin = new Vector2(600f,300f);
    }

    public void SetBigWindow()
    {
        RectTransform editingPanelRectTransform = gameObject.GetComponent<RectTransform>();
        editingPanelRectTransform.offsetMax = new Vector2(-100f,-280f);
        editingPanelRectTransform.offsetMin = new Vector2(100f,280f);
    }
    
    public void SetDeletingWindow()
    {
        RectTransform editingPanelRectTransform = gameObject.GetComponent<RectTransform>();
        editingPanelRectTransform.offsetMax = new Vector2(-500f,-400f);
        editingPanelRectTransform.offsetMin = new Vector2(500f,400f);
    }

    public void SetAddingGenreWindow()
    {
        RectTransform editingPanelRectTransform = gameObject.GetComponent<RectTransform>();
        editingPanelRectTransform.offsetMax = new Vector2(-450f, -400f);
        editingPanelRectTransform.offsetMin = new Vector2(450f, 400f);
    }

}
