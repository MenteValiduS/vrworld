﻿using SFB;
using UnityEngine;
using UnityEngine.UI;

public class OpenFilePanel : MonoBehaviour
{
    public InputField exePath;
    public InputField imagePath;
    public InputField videoText;

    public void OpenDialogForExe()
    {
        string res = OpenFileDialog.OpenDialog(new[] { new ExtensionFilter("Приложение", "exe") });

        if (!res.Equals(string.Empty))
        {
            exePath.text = res;
        }
    }

    public void OpenDialogForImage()
    {
        string res = OpenFileDialog.OpenDialog(new[] { new ExtensionFilter("Изображение", "JPG", "PNG") });

        if (!res.Equals(string.Empty))
        {
            imagePath.text = res;
        }
    }


    public void OpenDialogForVideo()
    {
        string res = OpenFileDialog.OpenDialog(new[] { new ExtensionFilter("Видео", "MP4", "AVI") });

        if (!res.Equals(string.Empty))
        {
            videoText.text = res;
        }
    }

}
