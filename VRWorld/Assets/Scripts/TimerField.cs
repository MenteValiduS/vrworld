﻿using Assets.Scripts.Models.DAO;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TimerField : MonoBehaviour
{
    private const string startingText = "Запускается";
    private const string endingText = "Завершается";
    private const string inGameText = "В игре";
    private const string stoppedText = "Приостановлено";

    [SerializeField]
    private GameObject causeMessage_Field;
    [SerializeField]
    private GameObject gameController_Field;

    private bool started;
    private bool closed;
    private Text gameStatusText;
    private Text remainingTimeText;
    private double lastCheckedTime;
    private double endTime;

    private void Start()
    {
        started = true;
        closed = false;
        remainingTimeText = GameObject.Find("RemainingTimeText").GetComponent<Text>();
        gameStatusText = GameObject.Find("StatusValueText").GetComponent<Text>();
        gameStatusText.text = startingText;
        lastCheckedTime = Timer.Time;
        endTime = Timer.Time + GameTimeManager.PlayTime;
    }

    private void Update()
    {

        if (gameStatusText.text.Equals(startingText) && (lastCheckedTime + 3d - Timer.Time) < double.Epsilon)
        {
            gameStatusText.text = inGameText;
            lastCheckedTime = Timer.Time;
        }
        //else if (gameStatusText.text.Equals(endingText) && (lastCheckedTime + 2d - Timer.Time) < double.Epsilon)
        //{
        //    Close();
        //}

        if (started)
        {
            ShowRemainingTime();
        }

        if (endTime - Timer.Time < double.Epsilon)
        {
            Stop();
        }

        if (!Check() && !closed)
        {
            StartCoroutine(CheckIfAppWillBeOpened());
        }

    }

    private IEnumerator CheckIfAppWillBeOpened()
    {
        double lastTime = Timer.Time;
        double newTime = Timer.Time + 5d;

        while (lastTime < newTime)
        {
            lastTime += Time.deltaTime;
            yield return null;
        }

        if (!Check())
        {
            SessionDAO.Add(ChooseGame.CurrentButton.GetComponent<GameButton>().name, GameTimeManager.PlayTime, DateTime.Now.ToShortDateString(), "Игра была завершена беспричинно.");
            Close();
        }
    }

    public void PauseOrContinue()
    {
        Image button = GameObject.Find("ContinuePauseButton").GetComponent<Image>();

        if (started)
        {
            Timer.Paused = true;
            button.sprite = Resources.Load<Sprite>("Sprites/Buttons/ContinueGameButtonImage");
            gameStatusText.text = stoppedText;
            started = false;
        }
        else
        {
            Timer.Paused = false;
            button.sprite = Resources.Load<Sprite>("Sprites/Buttons/PauseGameButtonImage");
            gameStatusText.text = inGameText;
            started = true;
        }

    }

    public void Stop()
    {
        string processName = System.IO.Path.GetFileNameWithoutExtension(ChooseGame.CurrentButton.GetComponent<GameButton>().pathToExe);

        foreach (System.Diagnostics.Process process in System.Diagnostics.Process.GetProcesses())
        {

            if (process.ProcessName.Contains(processName))
            {
                process.Kill();
            }

        }

        Timer.Paused = true;
        Advertisement.ToUpdate = true;
        closed = true;
        gameStatusText.text = endingText;
        lastCheckedTime = Timer.Time;
        //DisableField();
        //if (succeed)
        //{
            SessionDAO.Add(ChooseGame.CurrentButton.GetComponent<GameButton>().name, GameTimeManager.PlayTime, DateTime.Now.ToShortDateString(), "Успешно завершено.");
            Close();
        //}
        //else
        //{
        //    causeMessage_Field.SetActive(true);
        //}
    }

    

    private bool Check()
    {
        string processName = System.IO.Path.GetFileNameWithoutExtension(ChooseGame.CurrentButton.GetComponent<GameButton>().pathToExe);

        foreach (System.Diagnostics.Process process in System.Diagnostics.Process.GetProcesses())
        {

            if (process.ProcessName.Contains(processName))
            {
                return true;
            }

        }

        return false;
    }

    private void DisableField()
    {
        Button continuePauseButton = GameObject.Find("ContinuePauseButton").GetComponent<Button>();
        Button stopButton = GameObject.Find("StopButton").GetComponent<Button>();

        continuePauseButton.enabled = false;
        stopButton.enabled = false;
    }

    private void Close()
    {
        Timer.Paused = false;
        PlayGame.Launched = false;
        Destroy(GameObject.Find("CoveringField(Clone)"));
        Destroy(gameObject);
    }

    private void ShowRemainingTime()
    {
        int remainingTime = (int)(endTime - Timer.Time);
        remainingTimeText.text = string.Format("{0}мин {1}с", ((int)(remainingTime / 60d)).ToString(), ((int)(remainingTime % 60d)).ToString());
    }

}
