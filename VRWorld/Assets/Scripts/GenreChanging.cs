﻿using UnityEngine;

public class GenreChanging : AreaChooser
{
    [SerializeField]
    private GameObject lastField;
    [SerializeField]
    private GameObject addingGenreArea;
    [SerializeField]
    private GameObject deletingGenreArea;

    public void AddGenre()
    {
        parentArea.GetComponent<EditingPanel>().SetAddingGenreWindow();
        Instantiate(addingGenreArea, parentArea.transform);
        CloseLastArea();
    }


    public void DeleteGenre()
    {
        parentArea.GetComponent<EditingPanel>().SetDeletingWindow();
        Instantiate(deletingGenreArea, parentArea.transform);
        CloseLastArea();
    }

    public void Back()
    {
        Destroy(gameObject);
        GameObject editingPanelObject = GameObject.Find("EditingPanel(Clone)");
        Instantiate(lastField, editingPanelObject.transform);
    }

}
