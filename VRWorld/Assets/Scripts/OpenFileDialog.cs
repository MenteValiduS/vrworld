﻿using SFB;
using UnityEngine;
using UnityEngine.UI;

public class OpenFileDialog : MonoBehaviour
{

    public static string OpenDialog(ExtensionFilter[] extensionFilters)
    {
        var paths = StandaloneFileBrowser.OpenFilePanel("Укажите путь к файлу", "", extensionFilters, false);

        foreach (string path in paths)
        {
            return path;
        }

        return string.Empty;
    }

}
