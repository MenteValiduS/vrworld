﻿using UnityEngine;
using UnityEngine.UI;

public class AddingGenreArea : AreaChooser
{
    [SerializeField]
    private GameObject lastField;
    [SerializeField]
    private InputField genreNameText;
    [SerializeField]
    private InputField descriptionText;

    public void AddGenre()
    {
        if (Check())
        {
            Genre genre = new Genre(genreNameText.text, descriptionText.text);
            GenresDAO.Add(genre);
            GenrePanel.IsToUpdate = true;
            MessageSystem.MessageText = "Жанр успешно добавлен!";
            MessageSystem.ShowDialog();
            GenresDAO.SaveGenres();
            Clear();
        }
        else
        {
            MessageSystem.IsToShow = true;
            return;
        }
    }

    private void Clear()
    {
        genreNameText.text = string.Empty;
        genreNameText.placeholder.color = Color.gray;
        genreNameText.textComponent.color = Color.black;
        descriptionText.text = string.Empty;
        descriptionText.placeholder.color = Color.gray;
    }

    private new void Close()
    {
        Destroy(GameObject.Find("EditingPanel(Clone)"));
        Destroy(GameObject.Find("CoveringField(Clone)"));
    }

    private bool Check()
    {

        if (genreNameText.text.Equals(string.Empty))
        {
            genreNameText.placeholder.color = Color.red;
            MessageSystem.MessageText = "Введите название жанра!";
            return false;
        }
        else if (GenresDAO.Exists(genreNameText.text) || genreNameText.text.Equals("Все"))
        {
            MessageSystem.MessageText = "Такой жанр уже существует!";
            return false;
        } else
        {
            genreNameText.placeholder.color = Color.gray;
            genreNameText.textComponent.color = Color.black;
        }

        return true;
    }

    public void Back()
    {
        Destroy(gameObject);
        GameObject editingPanelObject = GameObject.Find("EditingPanel(Clone)");
        EditingPanel editingPanel = editingPanelObject.GetComponent<EditingPanel>();
        editingPanel.SetSmallWindow();
        Instantiate(lastField, editingPanelObject.transform);
    }

}
