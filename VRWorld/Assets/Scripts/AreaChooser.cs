﻿using UnityEngine;

public class AreaChooser : MonoBehaviour
{
    protected GameObject parentArea;
    protected GameObject thisArea;
    protected GameObject coveringField;

    private void Start()
    {
        parentArea = GameObject.Find("EditingPanel(Clone)");
        thisArea = this.gameObject;
    }

    public void Close()
    {
        //Destroy(GameObject.Find("EditingPanel(Clone)"));
        Destroy(GameObject.Find("CoveringField(Clone)"));
    }

    protected void CloseLastArea()
    {
        Destroy(thisArea);
    }
}
