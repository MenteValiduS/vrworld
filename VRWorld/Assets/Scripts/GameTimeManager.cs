﻿using UnityEngine;
using UnityEngine.UI;

public class GameTimeManager : MonoBehaviour
{
    [SerializeField]
    private InputField timeInputField;
    private static int playTime;
    private static bool isToUpdate;

    public static int PlayTime
    {
        get
        {
            return playTime;
        }

        private set
        {
            playTime = value;
        }
    }

    private void Start()
    {
        timeInputField.text = SettingsDAO.SettingsContainer.settings.gameTime.ToString();
        PlayTime = int.Parse(timeInputField.text) * 60;
        isToUpdate = false;
    }

    private void Update() 
    {
        if (isToUpdate)
        {
            timeInputField.text = (PlayTime / 60).ToString();
            isToUpdate = false;
        }
    }

    public void OnEndTextEdit()
    {

        if (!timeInputField.text.Equals(string.Empty))
        {
            int time = int.Parse(timeInputField.text);

            if (time > 0)
            {
                PlayTime = int.Parse(timeInputField.text) * 60;
            }
            else
            {
                timeInputField.text = SettingsDAO.SettingsContainer.settings.gameTime.ToString();
            }

        } else
        {
            timeInputField.text = SettingsDAO.SettingsContainer.settings.gameTime.ToString();
        }

    }

    public static void SetGameTime(int newTime)
    {
        PlayTime = newTime * 60;
        isToUpdate = true;
    }
}
