﻿using UnityEngine;
using UnityEngine.UI;

public class PreviewField : MonoBehaviour
{
    [SerializeField]
    private InputField exeInput;
    [SerializeField]
    private InputField imageInput;
    [SerializeField]    
    private InputField trailerInput;
    [SerializeField]
    private Text exeText;
    [SerializeField]
    private Image image;
    [SerializeField]
    private RawImage trailer;

    public void SetAll()
    {
        SetExe();
        SetImage();
        SetTrailer();
    }

    public void SetExe()
    {

        if (System.IO.File.Exists(exeInput.text))
        {
            exeText.text = System.IO.Path.GetFileName(exeInput.text);
        }

    }

    public void SetImage()
    {

        if (System.IO.File.Exists(imageInput.text))
        {
            image.sprite = IMG2Sprite.LoadNewSprite(imageInput.text);
        }

    }

    public void SetTrailer()
    {

        if (System.IO.File.Exists(trailerInput.text))
        {
            gameObject.GetComponent<Player>().Play(trailerInput.text, gameObject.GetComponent<UnityEngine.Video.VideoPlayer>(),
                trailer);
        }

    }

    public void Clear()
    {
        exeText.text = string.Empty;
        image.sprite = null;
        gameObject.GetComponent<UnityEngine.Video.VideoPlayer>().Stop();
    }
}
