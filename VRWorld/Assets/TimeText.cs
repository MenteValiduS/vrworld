﻿using UnityEngine;
using UnityEngine.UI;

public class TimeText : MonoBehaviour
{
    private Text timeText;
    void Start()
    {
        timeText = gameObject.GetComponent<Text>();
    }

    
    void Update()
    {
        string newTimeText = ShowTime();

        if (!timeText.text.Equals(newTimeText))
        {
            timeText.text = newTimeText;
        }
    }

    public string ShowTime()
	{
		return System.DateTime.Now.ToString("HH:mm");
	}
}
