﻿using System.Collections;
using System.Collections.Generic;
using Mono.Data.Sqlite;
using System.Data;
using UnityEngine;
using UnityEngine.UI;

public class test : MonoBehaviour
{
    private static string connectionString;

    [SerializeField]
    private InputField id;
    [SerializeField]
    private InputField name;
    [SerializeField]
    private InputField description;

    private void Awake()
    {
        connectionString = "URI=file:" + System.IO.Path.Combine(Application.dataPath, "VWDB.db");
    }


    public void Insert()
    {
        using (var connection = new SqliteConnection(connectionString))
        {
            connection.Open();

            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "INSERT INTO Genres (name, description)" +
                    "VALUES (@name, @descr)";

               // cmd.Parameters.Add(new SqliteParameter("id", id.text));
                cmd.Parameters.Add(new SqliteParameter("name", name.text));
                cmd.Parameters.Add(new SqliteParameter("descr", description.text));

                var result = cmd.ExecuteNonQuery();
                Debug.Log(result);
            }
        }
    }

    public void Select()
    {
        using (var connection = new SqliteConnection(connectionString))
        {
            connection.Open();

            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT name, description " +
                    "FROM Genres " +
                    "WHERE ID=@name";

                cmd.Parameters.Add(new SqliteParameter("name", int.Parse(id.text)));
                var reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    name.text = reader.GetString(0);
                    description.text = reader.GetString(1);
                }
            }

        }
    }

    public void Delete()
    {
        using (var connection = new SqliteConnection(connectionString))
        {
            connection.Open();

            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "DELETE FROM Genres " +
                    "WHERE ID=@id";
                cmd.Parameters.Add(new SqliteParameter("id", int.Parse(id.text)));

                cmd.ExecuteNonQuery();
            }
        }
    }
}
